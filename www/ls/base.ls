return if window.location.hash == '#vek'
class Col
  (@name, @field, @fieldPerc) ->
display = "cinnost"
cols =
  new Col "doma" "doma" "doma_perc"
  new Col "škola" "škola" "škola_perc"
  new Col "sport" "sport" "sport_perc"
  new Col "doprava" "doprava" "doprava_perc"

data = d3.tsv.parse ig.data[display], (row) ->
  for field, value of row
    continue if field == "popis"
    row[field] = parseFloat value.replace ',' '.'
  for col in cols
    row[col.fieldPerc] = row[col.field] / row.celkem
  row.isLong = row.popis.length > 30
  row

data.sort (a, b) -> b.celkem - a.celkem
lineHeight = 50
radius = d3.scale.sqrt!
  ..domain [0 d3.max data.map (.celkem)]
  ..range [0 lineHeight - 15]
colHeight = d3.scale.linear!

for datum, index in data
  datum.index = index

headerHeight = 50

container = d3.select ig.containers.base
container.append \ul
  ..append \li
    ..attr \class \header
    ..append \div
      ..attr \class "total"
      ..attr \data-sorter "celkem"
      ..html "Počet úrazů"
    ..append \div
      ..attr \class \sort-tip
      ..html "Kliknutím na záhlaví sloupce<br>můžete úrazy seřadit"
    ..append \div
      ..attr \class "cols topline"
      ..html "Místo, kde se úraz nejčastěji stává"
    ..append \div
      ..attr \class "cols headers"
      ..selectAll \div .data cols .enter!append \div
        ..attr \class "col"
        ..attr \data-sorter (.fieldPerc)
        ..html (.name)

  ..selectAll \li.body .data data .enter!append \li
    ..attr \class \body
    ..style \top -> "#{headerHeight + lineHeight * it.index}px"
    ..append \span
      ..attr \class \name
      ..html (.popis)
      ..classed \long (.isLong)
    ..append \div
      ..attr \class \total
      ..style \width -> "#{radius it.celkem}px"
      ..style \height -> "#{radius it.celkem}px"
      ..append \div
        ..attr \class \count
        ..style \right -> "#{10 + 1.1 * (radius it.celkem)}px"
        ..html -> ig.utils.formatNumber it.celkem
    ..append \div
      ..attr \class \cols
      ..selectAll \div .data cols .enter!append \div
        ..attr \class \col
        ..append \div
          ..attr \class \area
          ..style \height (col, i, ii) -> "#{data[ii][col.fieldPerc] * 100}%"
          ..classed \small (col, i, ii) -> data[ii][col.fieldPerc] < 0.5
          ..append \span
            ..html (col, i, ii) -> "#{Math.round data[ii][col.fieldPerc] * 100} %"

container.selectAll "[data-sorter]"
  .on \click ->
    d3.event.preventDefault!
    field = @getAttribute \data-sorter
    data.sort (a, b) -> b[field] - a[field]
    for datum, index in data
      datum.index = index
    container.selectAll \li.body
      ..style \top -> "#{headerHeight + lineHeight * it.index}px"
