return unless window.location.hash == '#vek'
class Col
  (@name, @field, @fieldPerc) ->
display = "vek"
cols =
  new Col "0-1" "0-1" "0-1_perc"
  new Col "2-4" "2-4" "2-4_perc"
  new Col "5-9" "5-9" "5-9_perc"
  new Col "10-14" "10-14" "10-14_perc"
  new Col "15-19" "15-19" "15-19_perc"

data = d3.tsv.parse ig.data[display], (row) ->
  for field, value of row
    continue if field == "popis"
    row[field] = parseFloat value.replace ',' '.'
  for col in cols
    row[col.fieldPerc] = row[col.field] / row.celkem
  row.isLong = row.popis.length > 30
  row
data.sort (a, b) -> a.ordering - b.ordering
console.log data.0
lineHeight = 50
radius = d3.scale.sqrt!
  ..domain [0 d3.max data.map (.celkem)]
  ..range [0 lineHeight - 15]
colHeight = d3.scale.linear!

for datum, index in data
  datum.index = index

headerHeight = 50

container = d3.select ig.containers.base
container.append \ul
  ..attr \class \vek
  ..append \li
    ..attr \class \header
    ..append \div
      ..attr \class \sort-tip
      ..html "Kliknutím na záhlaví sloupce<br>můžete úrazy seřadit"
    ..append \div
      ..attr \class "cols topline"
      ..html "Věk při kterém se úraz stává"
    ..append \div
      ..attr \class "cols headers"
      ..selectAll \div .data cols .enter!append \div
        ..attr \class "col"
        ..attr \data-sorter (.fieldPerc)
        ..html (.name)

  ..selectAll \li.body .data data .enter!append \li
    ..attr \class \body
    ..style \top -> "#{headerHeight + lineHeight * it.index}px"
    ..append \span
      ..attr \class \name
      ..html (.popis)
      ..classed \long (.isLong)
    ..append \div
      ..attr \class \cols
      ..selectAll \div .data cols .enter!append \div
        ..attr \class \col
        ..append \div
          ..attr \class \area
          ..style \height (col, i, ii) -> "#{data[ii][col.fieldPerc] * 100}%"
          ..classed \small (col, i, ii) -> data[ii][col.fieldPerc] < 0.5
          ..append \span
            ..html (col, i, ii) -> "#{Math.round data[ii][col.fieldPerc] * 100} %"

container.selectAll "[data-sorter]"
  .on \click ->
    d3.event.preventDefault!
    field = @getAttribute \data-sorter
    data.sort (a, b) -> b[field] - a[field]
    for datum, index in data
      datum.index = index
    container.selectAll \li.body
      ..style \top -> "#{headerHeight + lineHeight * it.index}px"
